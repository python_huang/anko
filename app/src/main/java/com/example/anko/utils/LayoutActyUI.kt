package com.example.anko.utils

import com.example.anko.activity.LayShowActivity
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk27.coroutines.onClick

class LayoutActyUI : AnkoComponent<LayShowActivity> {

    private val ET_ID = 0x1001

    override fun createView(ui: AnkoContext<LayShowActivity>) = with(ui) {
        verticalLayout {

            val name = editText("LayoutActyUI") {
                id = ET_ID
            }

            button("Say Hello") {
                onClick {
                    ctx.toast("Hello, ${name.text}!")
                    name.textColor = 0xffff0000.toInt()
                }

            }
        }
    }

}