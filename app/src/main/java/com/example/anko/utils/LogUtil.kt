package com.example.anko.utils

import org.jetbrains.anko.*

object LogUtil : AnkoLogger {

     fun av(str: Any?) {
        verbose(str)
    }

     fun ad(str: Any?) {
        debug(str)
    }

     fun  ai(str: Any?) {
        info(str)
    }

     fun aw(str: Any?) {
        warn(str)
    }

     fun ae(str: Any?) {
        error(str)
    }

}