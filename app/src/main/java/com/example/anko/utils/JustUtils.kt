package com.example.anko.utils

import com.example.anko.utils.LogUtil.ai
import java.util.concurrent.FutureTask

fun doBgWork(): String {
    val futureTask = FutureTask<String> {
        ai("sleep in "+Thread.currentThread().name)
        Thread.sleep(1000)
        "sleep over"
    }
    futureTask.run()
    return futureTask.get()
}