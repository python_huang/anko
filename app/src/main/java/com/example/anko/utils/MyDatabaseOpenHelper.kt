package com.example.anko.utils

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.example.anko.utils.LogUtil.ai
import org.jetbrains.anko.db.*

/**
 * 注意數據庫降級可能報錯
 */
class MyDatabaseOpenHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx, "MyDatabase", null, 1) {
    companion object {
        private var instance: MyDatabaseOpenHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): MyDatabaseOpenHelper {
            if (instance == null) {
                instance = MyDatabaseOpenHelper(ctx.applicationContext)
            }
            return instance!!
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
        // Here you create tables
        db.createTable("test", true, "id" to INTEGER + PRIMARY_KEY + UNIQUE,
                "name" to TEXT,
                "age" to INTEGER
        )
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        if (newVersion == 2) {
            if (oldVersion < newVersion) {
                ai("newVersion--$newVersion")
                db.execSQL("  ALTER TABLE Person RENAME TO temp_person")
                db.createTable("Person", true, "id" to INTEGER + PRIMARY_KEY + UNIQUE,
                        "name" to TEXT,
                        "age" to INTEGER,
                        "address" to TEXT,
                        "sex" to INTEGER
                )
                db.execSQL("INSERT INTO Person SELECT id, name, age,address,'' FROM temp_person")
                db.dropTable("temp_person")
            }
        }
    }
}

// Access property for Context
val Context.database: MyDatabaseOpenHelper
    get() = MyDatabaseOpenHelper.getInstance(applicationContext)

