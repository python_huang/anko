package com.example.anko.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.anko.R
import kotlinx.android.synthetic.main.activity_intent.*

class IntentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent)
        val name = intent.extras?.getString("name")
        val age = intent.extras?.getInt("age")
        textShow.text = "name:$name,age:$age"
    }
}
