package com.example.anko.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.anko.R
import com.example.anko.utils.LogUtil.ad
import com.example.anko.utils.LogUtil.ae
import com.example.anko.utils.LogUtil.ai
import com.example.anko.utils.LogUtil.av
import com.example.anko.utils.LogUtil.aw
import com.example.anko.utils.doBgWork
import kotlinx.android.synthetic.main.activity_common.*
import org.jetbrains.anko.*

class CommonActivity : AppCompatActivity(), AnkoLogger {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_common)

        textToIntent1.setOnClickListener {
            startActivity<IntentActivity>("name" to "Jack", "age" to 12)
        }

        textToIntent2.setOnClickListener {
            startActivity(intentFor<IntentActivity>("name" to "Lily", "age" to 13).singleTop())
        }

        textToBrowser.setOnClickListener {
            browse("http://m.baidu.com")
        }

        textToLog.setOnClickListener {
            verbose("tag-default class")//unable to print
            debug(110)
            warn(null)
            info(listOf("today", "is", "a", "fine", "day"))
            error(HashMap<String, String>().apply {
                put("Jack", "12")
                put("Lily", "13")
            })
            //other
            av("tag-default class")
            ad("debug")
            aw(null)
            ai(listOf("today", "is", "a", "fine", "day"))
            ae(HashMap<String, String>().apply {
                put("Jack", "12")
                put("Lily", "13")
            })
        }

        textToDimension.setOnClickListener {
            //context,ankocontext,fragment,view can be used all
            info(dip(100))//dp->px
            info(px2dip(100))//px->dp
        }

        textToHelpers.setOnClickListener {
            val builder = StringBuilder().apply {
                append("displayMetrics.widthPixels:$displayMetrics\n")
            }.toString()
            info(builder)
            info(attempt { 3 }.value)//substitute for try..catch?
            info(attempt { 0 }.error)//
            doFromSdk(21) {
                info("print from api 21")
            }
            doIfSdk(21) {
                packageManager.getPackageInfo(packageName, 0).versionName
                info("print only api is 21")
            }
            doAsync {
                val str = doBgWork()
                uiThread { info(Thread.currentThread().name + " $str") }
            }
        }

        textToAlert.setOnClickListener {
            alert("this is the msg", "title") {
                yesButton { toast("button-yes") }
                noButton { toast("button-no") }
            }.show()
        }

        textToAlert2.setOnClickListener {

            alert("this is the msg") {
                customTitle {
                    verticalLayout {
                        imageView(R.mipmap.ic_launcher)
                        editText { hint = "hint_title" }
                    }
                }
                okButton { toast("button-ok") }
                cancelButton { toast("button-cancel") }
            }.show()
        }

        textToSelector.setOnClickListener {
            val countries = listOf("Russia", "USA", "England", "Australia")
            selector("Where are you from?", countries) { _, i ->
                toast("So you're living in ${countries[i]}, right?")
            }
        }
    }

}